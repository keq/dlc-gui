.. dlc-gui documentation master file, created by
   sphinx-quickstart on Sat Jan 26 15:27:00 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dlc-gui's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents:

   README
   CHANGELOG
