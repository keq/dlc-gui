from PySide2.QtCore import QTimer, SLOT

from hypothesis import given, settings, HealthCheck
import pytest

from tests.mock_project import ModelProject
import tests.strategies as tst

import dlc_gui.gui
import dlc_gui.util

mp = ModelProject()


@pytest.fixture
def main_window(qtbot):
    main_window = dlc_gui.gui.MainWindow(mp.CONFIG_PATH, 0.2)
    main_window.show()
    qtbot.addWidget(main_window)

    return main_window


@pytest.fixture
def main_widget(main_window, qtbot):
    main_widget = main_window.main_widget
    qtbot.addWidget(main_widget)
    return main_widget


@given(path=tst.none_and_paths())
@settings(deadline=None, suppress_health_check=[HealthCheck.too_slow])
def test_init_from_random_paths(path, main_widget):
    main_widget.init_from_data_model_from_file(path)
    main_widget.init_from_data_model_from_dir(path)


def test_window_title(main_window, qtbot):
    assert main_window.windowTitle() == "DeepLabCut Labeling GUI"


def test_save_dialog(main_widget):
    QTimer.singleShot(500, main_widget.save_file_dialog, SLOT("close()"))
    main_widget.save_as_pkl()
    QTimer.singleShot(500, main_widget.save_file_dialog, SLOT("close()"))
    main_widget.save_as_hdf()


def test_open_dialog(main_window):
    QTimer.singleShot(500, main_window.open_file_dialog, SLOT("close()"))
    main_window.open_frames_dir.trigger()
    QTimer.singleShot(500, main_window.open_file_dialog, SLOT("close()"))
    main_window.open_dataframe_file.trigger()


def test_zoom(main_widget):
    # There is no simulation of mouse wheel yet:
    # https://bugreports.qt.io/browse/QTBUG-71449
    main_widget.init_from_data_model_from_file(mp.H5_FILE_PATH)
    zoom_factor = 600
    main_widget.graphics_view.zoom((0, 0), zoom_factor, anchor=0)
    assert main_widget.graphics_view.current_scale == zoom_factor


def test_wasd(main_widget, qtbot):

    bodyparts_count = len(main_widget.bodyparts)
    for row in range(bodyparts_count - 1):
        assert main_widget.current_bodypart_row == row
        qtbot.keyClick(main_widget, "d", delay=200)

    # TODO find out why property setting doesn't work here
    # main_widget.current_bodypart_row = bodyparts_count

    for row in range(bodyparts_count):
        assert main_widget.current_bodypart_row == bodyparts_count - row - 1
        qtbot.keyClick(main_widget, "a", delay=200)

    main_widget.init_from_data_model_from_file(mp.H5_FILE_PATH)

    frames_count = len(list(main_widget.data_model.frames_dict.values()))

    for row in range(frames_count - 1):
        assert main_widget.current_frame_row == row
        qtbot.keyClick(main_widget, "s", delay=200)

    for row in range(frames_count):
        assert main_widget.current_frame_row == frames_count - row - 1
        qtbot.keyClick(main_widget, "w", delay=200)
