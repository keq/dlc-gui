from tests.mock_project import ModelProject

import dlc_gui.util

mp = ModelProject()


model_config_dict = {
    "Task": "sample",
    "TrainingFraction": [0.95],
    "alphavalue": 0.5,
    "batch_size": 4,
    "bodyparts": ["bp0", "bp1", "bp2", "bp3", "bp4", "bp5", "bp6", "bp7", "bp8", "bp9"],
    "colormap": "jet",
    "corner2move2": (50, 50),
    "cropping": False,
    "date": "Jan27",
    "dotsize": 12,
    "iteration": 0,
    "move2corner": True,
    "numframes2pick": 20,
    "pcutoff": 0.1,
    "project_path": 0,
    "resnet": 50,
    "scorer": "Scorer",
    "snapshotindex": -1,
    "start": 0,
    "stop": 1,
    "video_sets": {"./videos/kitten_stock_footage.mp4": {"crop": "0, 1920, 0, 1080"}},
    "x1": 0,
    "x2": 640,
    "y1": 277,
    "y2": 624,
}


# Due to pre-session editing of the config.yaml,
# this test essentially ignores the project_path key


config_dict = dlc_gui.util.read_config_file(mp.CONFIG_PATH)
config_dict["project_path"] = 0


def test():
    assert config_dict == model_config_dict
