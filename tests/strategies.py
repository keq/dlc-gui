from pathlib import Path

from hypothesis import strategies as st


@st.composite
def paths(draw) -> Path:
    return Path(
        *draw(
            st.lists(
                st.text(
                    max_size=16,
                    alphabet=st.characters(
                        blacklist_characters=u"\x00/?#", blacklist_categories=("Cs",)
                    ),
                ),
                min_size=1,
                max_size=10,
                unique=True,
            )
        )
    )


@st.composite
def none_and_paths(draw):
    return draw(st.one_of(st.none(), paths()))
